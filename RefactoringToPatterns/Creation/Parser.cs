namespace Creation
{
    public class Parser
    {
        private bool _shouldDecode;
        private bool _shouldeRemoveEscapeCharacters;

        public Parser()
        {
            _shouldDecode = false;
            _shouldeRemoveEscapeCharacters = false;
        }

        public bool ShouldDecodeNodes()
        {
            return _shouldDecode;
        }

        public bool ShouldRemoveEscapeCharacters()
        {
            return _shouldeRemoveEscapeCharacters;
        }

        public void SetNodeDecoding(bool shouldDecode)
        {
            _shouldDecode = shouldDecode;
        }

        public void SetRemoveEscapeCharacters(bool shouldRemoveEscapeChars)
        {
            _shouldeRemoveEscapeCharacters = shouldRemoveEscapeChars;
        }
    }
}