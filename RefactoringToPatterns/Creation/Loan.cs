﻿using System;
using Creation.Abstract;
using Creation.Strategies;

namespace Creation
{
    public class Loan
    {
        public ICapitalStrategy CapitalStrategy { get; }
        private double _commitment;
        private double _outstanding;
        private int _riskRating;
        private DateTime _maturity;
        private DateTime _expiry;

        public Loan(double commitment, int riskRating, DateTime maturity) : this(null, commitment, 0d, riskRating, maturity, new DateTime())
        {
        }

        public Loan(double commitment, int riskRating, DateTime maturity, DateTime expiry) : this(null, commitment, 0d, riskRating, maturity, expiry)
        {
        }

        public Loan(ICapitalStrategy capitalStrategy, double commitment, int riskRating, DateTime maturity, DateTime expiry) : this(capitalStrategy, commitment, 0d, riskRating, maturity, expiry)
        {
        }

        public Loan(ICapitalStrategy capitalStrategy, double commitment, double outstanding, int riskRating, DateTime maturity, DateTime expiry)
        {
            _commitment = commitment;
            _outstanding = outstanding;
            _riskRating = riskRating;
            _maturity = maturity;
            _expiry = expiry;
            CapitalStrategy = capitalStrategy;

            if (CapitalStrategy == null)
            {
                if (_expiry == DateTime.MinValue)
                {
                    CapitalStrategy = new CapitalStrategyTermLoan();
                }
                else if (_maturity == DateTime.MinValue)
                {
                    CapitalStrategy = new CapitalStrategyRevolver();
                }
                else
                {
                    CapitalStrategy = new CapitalStrategyRCTL();
                }
            }
        }
    }
}
