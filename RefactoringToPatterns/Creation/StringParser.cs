﻿using Creation.Abstract;

namespace Creation
{
    public class StringParser
    {
        private readonly Parser _parser;

        public StringParser(Parser parser)
        {
            _parser = parser;
        }

        public INode Find(string textBuffer, string patternToFind)
        {
            const int textBegin = 0;
            var textEnd = textBuffer.Length;
            return new StringNode(textBuffer, textBegin, textEnd, _parser.ShouldDecodeNodes(), _parser.ShouldRemoveEscapeCharacters());
        }
    }
}