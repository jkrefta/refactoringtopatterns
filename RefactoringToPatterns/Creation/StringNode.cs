using Creation.Abstract;

namespace Creation
{
    public class StringNode : INode
    {
        private string _text;
        private int _textBegin;
        private int _textEnd;
        private bool _shouldDecodeNodes;
        private bool _shouldRemoveEscapeCharacters;

        public StringNode(string textBuffer, int textBegin, int textEnd, bool shouldDecodeNodes, bool shouldRemoveEscapeCharacters)
        {
            _text = textBuffer;
            _textBegin = textBegin;
            _textEnd = textEnd;
            _shouldDecodeNodes = shouldDecodeNodes;
            _shouldRemoveEscapeCharacters = shouldRemoveEscapeCharacters;
        }
    }
}