﻿using System;
using Creation.Strategies;
using NUnit.Framework;

namespace Creation.Tests
{
    [TestFixture]
    public class LoanTests
    {
        private const int Commitment = 100;
        private const int RiskRating = 100;

        [Test]
        public void TestTermLoanNoPayments()
        {
            var maturity = new DateTime(2015, 12, 10);
            var loan = new Loan(Commitment, RiskRating, maturity);
            Assert.That(loan.CapitalStrategy, Is.TypeOf<CapitalStrategyTermLoan>());
        }

        [Test]
        public void TestRCTLLoan()
        {
            var maturity = new DateTime(2015, 12, 10);
            var expiry = new DateTime(2015, 12, 20);
            var loan = new Loan(Commitment, RiskRating, maturity, expiry);
            Assert.That(loan.CapitalStrategy, Is.TypeOf<CapitalStrategyRCTL>());
        }

        [Test]
        public void TestRevloverLoan()
        {
            var expiry = new DateTime(2015, 12, 20);
            var loan = new Loan(Commitment, RiskRating, new DateTime(), expiry);
            Assert.That(loan.CapitalStrategy, Is.TypeOf<CapitalStrategyRevolver>());
        }

    }
}
